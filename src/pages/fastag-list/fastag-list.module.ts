import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FastagListPage } from './fastag-list';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FastagListPage,
  ],
  imports: [
    IonicPageModule.forChild(FastagListPage),
    TranslateModule.forChild()
  ],
})
export class FastagListPageModule {}
